gnome-shell-extension-suspend-button (0~git20191005-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.4.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 16 Mar 2020 03:44:19 +0000

gnome-shell-extension-suspend-button (0~git20191005-1) unstable; urgency=medium

  * New upstream git snapshot. (Closes: #941892).

 -- Tobias Frost <tobi@debian.org>  Mon, 07 Oct 2019 22:31:57 +0200

gnome-shell-extension-suspend-button (0~git20180827-2) unstable; urgency=medium

  * Recommend gnome-tweaks instead of gnome-tweak-tool. (Closes: #917785)

 -- Tobias Frost <tobi@debian.org>  Sun, 30 Dec 2018 12:11:19 +0100

gnome-shell-extension-suspend-button (0~git20180827-1) unstable; urgency=medium

  * New upstream git snapshot.
  * Bump SV to 4.3.0 -- no changes required

 -- Tobias Frost <tobi@debian.org>  Sat, 29 Dec 2018 23:50:34 +0100

gnome-shell-extension-suspend-button (0~git20171025-2) unstable; urgency=medium

  * Move repository to salsa.debian.org.
  * Bump d/compat to level 11.
  * Add watchfile to track commits.
     - add watch file
     - drop lintian override for "missing watch file"
     - drop get-orig-source in d/rules and remove usage of $PKG.
       As a side effect, this fixes also some lintian infos for using
       dpkg-parsechangelog.
  * Update d/changelog: Use https for the spec and update years.
  * Bump S-V to 4.1.4 -- no other changes required.

 -- Tobias Frost <tobi@debian.org>  Sun, 08 Apr 2018 01:31:56 +0200

gnome-shell-extension-suspend-button (0~git20171025-1) unstable; urgency=medium

  * New upstream git snapshot.
  * Bump SV to 4.1.1 -- no changes

 -- Tobias Frost <tobi@debian.org>  Thu, 26 Oct 2017 08:01:23 +0200

gnome-shell-extension-suspend-button (0~git20161006-1) unstable; urgency=medium

  * New upstream git snapshot.
  * Drop patch 01-fix-installdir.patch -- applied upstream
  * Drop upper version constraint on gnome-shell -- gnome-shell will
    ignore the extensions restrictions, so this is no longer needed
  * Update copyright years
  * Bump SV to 4.0.0 -- no changes needed

 -- Tobias Frost <tobi@debian.org>  Sun, 09 Jul 2017 14:30:36 +0200

gnome-shell-extension-suspend-button (0~git20160525-3) unstable; urgency=medium

  * locale *.mo creation is now done in upstream's Makefile
  * Do not install a copy of the locales to the extensions' dir
  * Do only install the compiled schema, not the xml source file
  * d/clean no longer needed, included in upstream Makefile
  * Bump d/compat to level 10

 -- Tobias Frost <tobi@debian.org>  Sun, 04 Dec 2016 15:36:39 +0100

gnome-shell-extension-suspend-button (0~git20160525-2) unstable; urgency=medium

  * d/control: Relax upper version limit on gnome-shell. (Closes: #837595)
    As the next Gnome version will by default not enforce the version via
    metadata.json, a change in this file is not required. See the bug for
    details.

 -- Tobias Frost <tobi@debian.org>  Wed, 14 Sep 2016 08:14:39 +0200

gnome-shell-extension-suspend-button (0~git20160525-1) unstable; urgency=medium

  * New upstream git snapshot.
  * Added patch 01-fix-installdir.patch to utilize DESTDIR for installation.
  * Added patch 02-do-not-install-COPYING-and-README.patch to avoid installing
    the files COPYING and README.md.

 -- Tobias Frost <tobi@debian.org>  Sun, 11 Sep 2016 09:40:40 +0200

gnome-shell-extension-suspend-button (0~git20160415-1) unstable; urgency=medium

  * New upstream git snapshot.
  * Supports Gnome 3.20
  * Bump SV to 3.9.8 -- no changes required
  * Change to https:// links for the Vcs-* Fields
  * Update copyright years
  * Remove 01-gnome318.patch series, no longer needed

 -- Tobias Frost <tobi@debian.org>  Tue, 26 Apr 2016 22:10:32 +0200

gnome-shell-extension-suspend-button (0~git20150615-2) unstable; urgency=medium

  * Add patch to supporrt Gnome 3.18 (Closes: #801645)

 -- Tobias Frost <tobi@debian.org>  Wed, 21 Oct 2015 21:51:52 +0200

gnome-shell-extension-suspend-button (0~git20150615-1) unstable; urgency=medium

  * New upstream git snapshot
  * Changing Depends to include GNOME 3.16
  * Update years in d/copyright and change to my debian.org email
  * Remove duplicate license text from d/copyright
  * Remove unneccessary cleanup lines from get-orig-source target in
    d/rules

 -- Tobias Frost <tobi@debian.org>  Tue, 16 Jun 2015 09:32:03 +0200

gnome-shell-extension-suspend-button (0~git20141024-1) unstable; urgency=medium

  * Initial release (Closes: #766342)

 -- Tobias Frost <tobi@debian.org>  Thu, 23 Oct 2014 13:44:33 +0200
